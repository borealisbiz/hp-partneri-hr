<?php

function get_base_url()
{
    $currentPath = $_SERVER['PHP_SELF'];

    $pathInfo = pathinfo($currentPath);

    $hostName = $_SERVER['HTTP_HOST'];

    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';

    return $protocol.$hostName.$pathInfo['dirname']."/";
}

function redirect($url) {
  header("Location: $url");
  die();
}

function view($name, $model = '') {
    global $view_bag;
    require(APP_PATH . "views/layout.view.php");
}

function admin_view($name, $model = '') {
    global $view_bag;
    require(APP_PATH . "views/admin/layout.view.php");
}

function is_get() {
  return $_SERVER['REQUEST_METHOD'] === 'GET';
}

function is_post() {
  return $_SERVER['REQUEST_METHOD'] === 'POST';
}

function sanitize($value) {
  $temp = filter_var(trim($value), FILTER_SANITIZE_STRING);

  if ($temp === false) {
    return '';
  }

  return $temp;
}
