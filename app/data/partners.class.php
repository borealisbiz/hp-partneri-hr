<?php

class Partners {
  public $id;
  public $name;
  public $address;
  public $postal_code;
  public $city;
  public $phone;
  public $category_id;
}
