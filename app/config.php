<?php

$lang = 'hr';

$dbconfig = [
  'hostname' => 'localhost',
  'dbname' => 'hp-hr',
  'dbport' => '3306',
  'dbuser' => 'root',
  'dbpass' => ''
];

$config = [
  'provider' => new MySqlDataProvider($dbconfig["hostname"], $dbconfig["dbname"], $dbconfig["dbport"], $dbconfig["dbuser"], $dbconfig["dbpass"]),
  'users' => new Users($dbconfig["hostname"], $dbconfig["dbname"], $dbconfig["dbport"], $dbconfig["dbuser"], $dbconfig["dbpass"]),
  'lang' => $lang
];
