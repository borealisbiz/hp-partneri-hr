<?php

// HP | Popis partnera / Hrvatska
$msg['Popis partnera | Hrvatska'] = 'Partnerji Slovenia';

$msg['Partneri'] = 'Partnerji';

//Prikaži _MENU_ partnera po stranici
$msg['Prikaži _MENU_ partnera po stranici'] = 'Prikaži _MENU_ partnerjev na stran';

$msg['Ništa nije pronađeno'] = 'Ništa nije pronađeno';
$msg['Pretraži'] = 'Poišči';
$msg['Partner'] = 'Partner';
$msg['Adresa'] = 'Mesto';
$msg['Grad'] = 'Grad';
$msg['Broj telefona'] = 'Telefonska številka';

// Prikazano _START_ do _END_ od ukupno _TOTAL_ partnera
$msg['Prikazano _START_ do _END_ od ukupno _TOTAL_ partnera'] = 'Prikazano _START_ do _END_ od skupno _TOTAL_ partnerjev';


//Prikazano 0 do 0 od ukupno _TOTAL_ partnera (filtrirano iz _MAX ukupnih rezultata)
$msg['Prikazano 0 do 0 od ukupno _TOTAL_ partnera'] = 'Prikazano 0 do 0 od skupno _TOTAL_ partnerjev';
$msg['filtrirano iz _MAX ukupnih rezultata'] = '(filtrirano iz _MAX_ ukupnih rezultata)';

$msg['Nazad'] = 'Nazaj';
$msg['Naprijed'] = 'Naprej';
