<?php

// HP | Popis partnera / Hrvatska
$msg['Popis partnera | Hrvatska'] = 'Popis partnera | Hrvatska';

$msg['Partneri'] = 'Popis partnera | Hrvatska';

//Prikaži _MENU_ partnera po stranici
$msg['Prikaži _MENU_ partnera po stranici'] = 'Prikaži _MENU_ partnera po stranici';

$msg['Ništa nije pronađeno'] = 'Ništa nije pronađeno';
$msg['Pretraži'] = 'Pretraži';
$msg['Partner'] = 'Partner';
$msg['Adresa'] = 'Adresa';
$msg['Grad'] = 'Grad';
$msg['Broj telefona'] = 'Broj telefona';

// Prikazano _START_ do _END_ od ukupno _TOTAL_ partnera
$msg['Prikazano _START_ do _END_ od ukupno _TOTAL_ partnera'] = 'Prikazano _START_ do _END_ od ukupno _TOTAL_ partnera';


//Prikazano 0 do 0 od ukupno _TOTAL_ partnera (filtrirano iz _MAX ukupnih rezultata)
$msg['Prikazano 0 do 0 od ukupno _TOTAL_ partnera'] = 'Prikazano 0 do 0 od ukupno _TOTAL_ partnera';
$msg['filtrirano iz _MAX ukupnih rezultata'] = '(filtrirano iz _MAX ukupnih rezultata)';

$msg['Nazad'] = 'Nazaj';
$msg['Naprijed'] = 'Naprej';
