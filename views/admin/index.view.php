<div id="tab1">
<div id="demo_trident">
<table cellpadding="0" cellspacing="0" border="0" class="display dataTable" id="allan">
<thead>
  <tr class="tableHeader">
    <th>Partner</th>
    <th>Adresa</th>
    <th>Grad</th>
    <th>Telefon</th>
    <th>Uredi/obriši</th>
  </tr>
</thead>
<tbody>


  <?php foreach ($model as $partner) : ?>

<tr class="gradeA">
  <td><?= $partner->name ?></td>
  <td><?= $partner->address ?></td>
  <td><?= $partner->postal_code ?> <?= $partner->city ?></td>
  <td><?= $partner->phone ?></td>
  <td><a href="edit.php?id=<?= $partner->id ?>">Uredi</a> / <a href="delete.php?id=<?= $partner->id ?>">Obriši</a></td>
</tr>

<?php endforeach; ?>

</table>
    </div></div>
</div>
