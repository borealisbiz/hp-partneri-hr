<div class="row">
  <div class="col-lg-12 text-center">
      <h1 class="mt-5">Obriši partnera</h1>
  </div>

</div>
  <div class="row align-items-center justify-content-center">
      Želite li stvarno obrisati <?= $model->name ?>?
  </div>
    <div class="row align-items-center justify-content-center">
        <form action="" method="POST" accept-charset="ISO-8859-1">
            <input type="hidden" name="id" value="<?= $model->id ?>" />
            <div class="form-group">
                <input type="submit" value="Obriši">
            </div>
        </form>
    </div>
