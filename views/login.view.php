<div class="container">
  <div class="row">
    <div class="col-lg-12 text-center">
      <h1 class="mt-5">Prijava</h1>
    </div>
  </div>
    <div class="row align-items-center justify-content-center">
      <div class="col-md-4">
      <form action="" method="POST">
        <div class="form-group">
          <label for="email">Email:</label>
          <input class="form-control" type="email" name="email" id="email" />
        </div>
        <div class="form-group">
          <label for="password">Lozinka:</label>
          <input class="form-control" type="password" name="pass" id="pass" />
        </div>
        <div class="from-group">
          <input class="form-control" type="submit" name="login" value="Prijava" />
        </div>
      </form>
      <?php if ($model != null) {
        echo $model["message"];
      } ?>
    </div>
  </div>
</div>
