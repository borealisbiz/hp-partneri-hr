<?php

session_start();
require('../app/app.php');

$config['users']->ensure_user_is_authenticated();

if (is_get()) {
  $key = sanitize($_GET['id']);

  if (empty($key)) {
    view('not_found');
    die();
  }

  $post = $config['provider']->get_partner($key);

  if ($post == false) {
    view('not_found');
    die();
  }

  admin_view('delete', $post);
}

if (is_post()) {
  $id = sanitize($_POST['id']);

  if (empty($id)) {
    echo "Ništa nije pronađeno";
  } else {
    $config['provider']->delete_partner($id);
    redirect('index.php');
  }
}
