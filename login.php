<?php

session_start();
require('app/app.php');

if ($config['users']->is_user_authenticated()) {
  redirect('admin/');
}


if (is_post()) {
  $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
  $pass = $_POST['pass'];
  $result = $config['users']->authenticate_user($email, $pass);

  if ($result["success"] == true) {
    redirect('admin/');
  } else {
    view('login', array(
      'message' => $result["message"]
    ));
  }
} else {
  view('login');
}
